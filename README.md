# sitespeed.io-client

With this little tool you can trigger a sitespeed test on a remote server running *sitespeed.io-server*.

This client uses a socket connection to remotely trigger a sitespeed.io test. The server will run the test and return its results.

## Requirements
Node.js needs to be installed.

## Installation
```npm install -g git+https://yesman82@bitbucket.org/yesman82/sitespeed.io-client.git```
You may have to use ```sudo``` powers.

This will install the client on a global level.

## Usage
```sitespeedioclient -h=my.server.com -u=http://www.example.com [-p=33445]```

Arguments:
- `-u` URL to test
- `-h` Hostname
- `-p` Port (optional, default: 33445)

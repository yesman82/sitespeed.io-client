/**
 * This client uses a socket connection to remotely trigger a
 * sitespeed.io test. The server will run the test and return
 * its results.
 */

/** @type {io} socket.io networking library  */
var io = require('socket.io-client');
/** @type {Table} Tool to create string tables */
var Table = require('cli-table');

/**
 * The sitespeed.io client
 * @param {String} host Host IP or domain of sitespeed.io server
 * @param {Number} port TCP port to talk to
 */
var SitespeedClient = function(host, port) {
    this.logger = typeof logger !== 'undefined' ? logger : console;

    // check for required hostname argument
    if (!host) {
        this.logger.error('No hostname specified.');
        return;
    }

    this.host = host;
    this.port = parseInt(port) || 33445;
};

/** @type {String} Host IP or domain */
SitespeedClient.prototype.host = null;

/** @type {Number} Port of server */
SitespeedClient.prototype.port = null;

/** @type {io.Socket} A socket.io socket connection */
SitespeedClient.prototype.socket = null;

/** @type {winston.logger|console} Winston logger instance or console */
SitespeedClient.prototype.logger = null;

/** @type {Function} Function executed when result was received (arguments: Object/JSON) */
SitespeedClient.prototype.onResult = function() {};

/** @type {Function} Function executed when connected to server */
SitespeedClient.prototype.onConnect = function() {};

/** @type {Function} Function executed when disconnected from server */
SitespeedClient.prototype.onDisconnect = function() {};

/** @type {Function} Function executed when error occured (arguments: Error object) */
SitespeedClient.prototype.onError = function() {};

/** @type {Function} Function executed when log entry recevied (arguments: level (String), message (String)) */
SitespeedClient.prototype.onLog = function() {};

/** @type {Function} Function executed when log entry recevied (arguments: level (String), message (String)) */
SitespeedClient.prototype.onQueuePosition = function() {};

/**
 * Trigger sitespeed.io test remotely.
 * @param  {Object/JSON} options Options to test with sitespeed.io
 */
SitespeedClient.prototype.runTest = function(options) {
    // check for required URL argument
    if (!options.url) {
        this.logger.error('No URL specified.');
        return;
    }

    this.logger.info('Connecting to ' + this.host + ':' + this.port + '.');
    this.socket = io('http://' + this.host + ':' + this.port, {transports: ['websocket'], reconnection: false});

    var logOptions = '';
    var keys = Object.keys(options);

    keys.forEach(function(key, index) {
        logOptions += key + ': ' + options[key] + (index !== keys.length - 1 ? '\n' : '');
    });

    this.logger.info(logOptions);

    // opening socket to server
    this.socket.on('connect', this.onSocketConnect.bind(this, options));
    this.socket.on('disconnect', this.onSocketDisconnect.bind(this));
    this.socket.on('log', this.onSocketDataLog.bind(this));
    this.socket.on('queuePosition', this.onSocketDataQueuePosition.bind(this));
    this.socket.on('result', this.onSocketDataResult.bind(this));
    this.socket.on('error', this.onSocketError.bind(this));
    this.socket.on('connect_error', this.onSocketError.bind(this));
    this.socket.on('connect_timeout', this.onSocketError.bind(this));
    this.socket.on('reconnect_error', this.onSocketError.bind(this));
    this.socket.on('reconnect_failed', this.onSocketError.bind(this));
};

/**
 * Log connection and run test.
 * @param  {Object/JSON} options Options to run test with
 */
SitespeedClient.prototype.onSocketConnect = function(options) {
    this.logger.info('Connected to ' + this.host + ':' + this.port);
    this.socket.emit('run', options);
    this.onConnect();
};

/**
 * Log disconnection and exit
 */
SitespeedClient.prototype.onSocketDisconnect = function() {
    this.logger.info('Disconnected from ' + this.host + ':' + this.port + '.');
    this.onDisconnect();
};

/**
 * Log error and exit.
 * @param  {String} error Error
 */
SitespeedClient.prototype.onSocketError = function(error) {
    this.logger.error('Error (' + this.host + ':' + this.port + '): ' + error.message);
    this.onError(error);
};

/**
 * Log winston message
 */
SitespeedClient.prototype.onSocketDataLog = function(log) {
    this.logger.log(log.level, log.message);
    this.onLog(log.level, log.message);
};

/**
 * Log winston message
 */
SitespeedClient.prototype.onSocketDataQueuePosition = function() {
    this.onQueuePosition(arguments);
};

/**
 * Show results
 * @param  {Object} result Object having yslow and timing data
 */
SitespeedClient.prototype.onSocketDataResult = function(result, socket) {
    var table = new Table({
            chars: {
                'mid': '',
                'left-mid': '',
                'mid-mid': '',
                'right-mid': ''
            },
            style: {
                head: ['cyan', 'white'],
                border: ['white']
            }
        }),
        yslow = result.yslow,
        assets = yslow && yslow.assets ? yslow.assets : null,
        timings = result.timings;

    if (yslow) {
        table.push({
            'Requests': [yslow.requests.v, yslow.requests.unit]
        }, {
            'Requests missing Expire': [yslow.requestsMissingExpire.v, yslow.requestsMissingExpire.unit]
        }, {
            'Page weight': [yslow.pageWeight.v, yslow.pageWeight.unit]
        }, {
            'Doc weight': [yslow.docWeight.v, yslow.docWeight.unit]
        }, {
            'JS': [assets.js.v, assets.js.unit]
        }, {
            'JS weight': [assets.jsWeight.v, assets.jsWeight.unit]
        }, {
            'CSS': [assets.css.v, assets.css.unit]
        }, {
            'CSS weight': [assets.cssWeight.v, assets.cssWeight.unit]
        }, {
            'Images': [assets.image.v, assets.image.unit]
        }, {
            'Image weight': [assets.imageWeight.v, assets.imageWeight.unit]
        }, {
            'CSS Images': [assets.cssimage.v, assets.cssimage.unit]
        }, {
            'CSS Image weight': [assets.cssimageWeight.v, assets.cssimageWeight.unit]
        }, {
            'Fonts': [assets.font.v, assets.font.unit]
        }, {
            'Font weight': [assets.fontWeight.v, assets.fontWeight.unit]
        }, {
            'Flash': [assets.flash.v, assets.flash.unit]
        }, {
            'Flash weight': [assets.flashWeight.v, assets.flashWeight.unit]
        }, {
            'iFrames': [assets.iframe.v, assets.iframe.unit]
        }, {
            'iFrame weight': [assets.iframeWeight.v, assets.iframeWeight.unit]
        });

    } else {
        this.logger.warn('No yslow and assets results');
    }

    if (timings) {
        table.push({
            'Back-End Time': [timings.backEndTime.min.v, timings.backEndTime.min.unit]
        }, {
            'Front-End Time': [timings.frontEndTime.min.v, timings.frontEndTime.min.unit]
        }, {
            'Page Load Time': [timings.pageLoadTime.min.v, timings.pageLoadTime.min.unit]
        }, {
            'DOM Content Loaded Time': [timings.domContentLoadedTime.min.v, timings.domContentLoadedTime.min.unit]
        }, {
            'First Paint': [timings.firstPaint.min.v, timings.firstPaint.min.unit]
        }, {
            'Speed Index': [timings.speedIndex.min.v, timings.speedIndex.min.unit]
        });

    } else {
        this.logger.warn('No timings results');
    }

    this.logger.info('Results:\r\n' + table);

    this.socket.disconnect();

    this.onResult(result);
};

module.exports = SitespeedClient;